﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endava.BuddySurfing.API.Models
{
    public class Buddy
    {
        public int id_buddy { get; set; } 
        public string name { get; set; }
        public string bio { get; set; }
        public bool hosting { get; set; }
        public bool guided_tour { get; set; }
        public bool invite_drink { get; set; }
        public string city_name { get; set; }
        public IList<string> languages { get; set; }
        public string bio_image_url { get; set; }
        public string email { get; set; }
        public string skype { get; set; }
    }
}
