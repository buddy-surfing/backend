﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endava.BuddySurfing.API.Models
{
    public class City
    {
        public string city_name { get; set; }
        public string country_name { get; set; }
        public string continent { get; set; }
        public string image_url { get; set; }
        public string image_large_url { get; set; }
    }
}
