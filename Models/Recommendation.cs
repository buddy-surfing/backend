﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endava.BuddySurfing.API.Models
{
    public class Recommendation
    {
        public int place_id { get; set; }
        public string place_name { get; set; }
        public List<string> tag_category { get; set; }
        public string address { get; set; }
        public string description { get; set; }
        public int like { get; set; }
        public string city_name { get; set; }
    }
}
