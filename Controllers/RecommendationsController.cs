﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endava.BuddySurfing.API.Builders;
using Endava.BuddySurfing.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Endava.BuddySurfing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecommendationsController : ControllerBase
    {
        private static IList<Recommendation> recomendations
            = new List<Recommendation>();

        public RecommendationsController()
        {
            recomendations = RecommendationBuilder.Build();
        }

        // GET api/recommendations/{city}
        [HttpGet]
        [Route("{city}")]
        public ActionResult<IEnumerable<Recommendation>> Get(string city)
        {
            var recommendationsByCity = recomendations
                .Where(f => string.Compare(city, f.city_name, StringComparison.OrdinalIgnoreCase) == 0)
                .ToList();

            return Ok(recommendationsByCity);
        }
    }
}
