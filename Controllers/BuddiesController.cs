﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endava.BuddySurfing.API.Builders;
using Endava.BuddySurfing.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Endava.BuddySurfing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuddiesController : ControllerBase
    {
        private static IList<Buddy> buddies = new List<Buddy>();

        public BuddiesController()
        {
            buddies = BuddiesBuilder.Build();
        }

        // GET api/Buddies/{city}
        [HttpGet]
        [Route("{city}")]
        public ActionResult<IList<Buddy>> Get(string city)
        {
            var buddiesByCity = buddies
                .Where(f => string.Compare(city, f.city_name, StringComparison.OrdinalIgnoreCase) == 0)
                .ToList();

            return Ok(buddiesByCity);
        }
    }
}
