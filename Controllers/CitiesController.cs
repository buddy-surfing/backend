﻿using Endava.BuddySurfing.API.Builders;
using Endava.BuddySurfing.API.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Endava.BuddySurfing.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        private static IList<City> cities = new List<City>();

        public CitiesController()
        {
            cities = CitiesBuilder.Build();
        }

        // GET api/cities
        [HttpGet]
        public ActionResult<IList<City>> Get()
        {
            return Ok(cities);
        }

        [HttpGet]
        [Route("{city}")]
        public ActionResult<City> GetByName(string city)
        {
            var citiesByCity = cities
                .FirstOrDefault(f => string.Compare(city, f.city_name, StringComparison.OrdinalIgnoreCase) == 0);

            return Ok(citiesByCity);
        }
    }
}
