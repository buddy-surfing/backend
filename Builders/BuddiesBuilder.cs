﻿using Endava.BuddySurfing.API.Models;
using System.Collections.Generic;

namespace Endava.BuddySurfing.API.Builders
{
    public static class BuddiesBuilder
    {
        public static IList<Buddy> Build()
        {
            return new List<Buddy>()
            {
                new Buddy()
                {
                  id_buddy=1,
                  name="Pablo Araya",
                  bio="You cannot hide forever, Luke. I will not fight you. Give yourself to the dark side. It is the only way you can save your friends. Yes, your thoughts betray you. Your feelings for them are strong. Especially for...",
                  hosting=true,
                  guided_tour=false,
                  invite_drink=false,
                  city_name="Rosario",
                  languages = new List<string>()
                  {
                    "Spanish",
                    "English"
                  },
                  bio_image_url="/buddies/pablo_araya.jpg",
                  email="pablo.araya@endava.com",
                  skype="pablomaraya"
                },

                new Buddy()
                {
                  id_buddy=2,
                  name="Alejandro Brizuela",
                  bio="Luke, we're ready for takeoff. Good luck, Lando When we find Jabba the Hut and that bounty hunter, we'll contact you. I'll meet you at the rendezvous point on Tatooine. Princess, we'll find Han. I promise. Chewie, I'll be waiting for your signal. Take care, you two. May the Force be with you. Ow!",
                  hosting=false,
                  guided_tour=true,
                  invite_drink=false,
                  city_name="Rosario",
                  languages = new List<string>()
                  {
                    "Spanish",
                    "English",
                    "Italian"
                  },
                  bio_image_url="/buddies/alejandro_brizuela.jpg",
                  email="alejandro.brizuela@endava.com",
                  skype="tortuycr"
                },

                new Buddy()
                {
                  id_buddy=3,
                  name="Manuel Bertelli",
                  bio="Stretch out with your feelings. You see, you can do it. I call it luck. In my experience, there's no such thing as luck. Look, going good against remotes is one thing. Going good against the living? That's something else. Looks like we're coming up on Alderaan. You know, I did feel something. I could almost see the remote. That's good. You have taken your first step into a larger world.",
                  hosting=false,
                  guided_tour=false,
                  invite_drink=true,
                  city_name="Amsterdam",
                  languages = new List<string>()
                  {
                    "Spanish",
                    "English"
                  },
                  bio_image_url="/buddies/manuel_bertelli.jpg",
                  email="manuel.bertelli@endava.com",
                  skype="manu_bertelli"
                },
                new Buddy()
                {
                  id_buddy=4,
                  name="Gabriel Giri",
                  bio="Master Yoda... is Darth Vader my father? Mmm... rest I need. Yes... rest. Yoda, I must know. Your father he is. Told you, did he? Yes. Unexpected this is, and unfortunate... Unfortunate that I know the truth? No. Unfortunate that you rushed to face him... that incomplete was your training. Not ready for the burden were you. Well, I'm sorry. Remember, a Jedi's strength flows from the Force. But beware. Anger, fear, aggression. The dark side are they. Once you start down the dark path, forever will it dominate your destiny. ",
                  hosting=true,
                  guided_tour=true,
                  invite_drink=false,
                  city_name="Amsterdam",
                  languages = new List<string>()
                  {
                    "Spanish"
                  },
                  bio_image_url="/buddies/gabriel_giri.jpg",
                  email="gabriel.giri@endava.com",
                  skype="gabrielgiri"
                },

                new Buddy()
                {
                  id_buddy=5,
                  name="Luciano Vitti",
                  bio="All wings report in. Red Ten standing by. Red Seven standing by. Red Three standing by. Red Six standing by. Red Nine standing by. Red Two standing by. Red Eleven standing by. Red Five standing by. Lock S-foils in attack position. We're passing through their magnetic field. Hold tight! Switch your deflectors on. Double front!",
                  hosting=true,
                  guided_tour=false,
                  invite_drink=true,
                  city_name="London",
                  languages = new List<string>()
                  {
                    "Spanish",
                    "English"
                  },
                  bio_image_url="/buddies/luciano_vitti.jpg",
                  email="luciano.vitti@endava.com",
                  skype="lvitti87"
                },

                new Buddy()
                {
                  id_buddy=6,
                  name="Tomás Novau",
                  bio="Come on! You want me to stay because of the way you feel about me. Yes. You're a great help to us. You're a natural leader... No! That's not it. Come on. Aahhh - uh huh! Come on. You're imagining things. Am I? Then why are you following me? Afraid I was going to leave without giving you a goodbye kiss? I'd just as soon kiss a Wookiee. I can arrange that. You could use a good kiss!",
                  hosting=true,
                  guided_tour=true,
                  invite_drink=true,
                  city_name="Bogotá",
                  languages = new List<string>()
                  {
                    "Spanish",
                    "English"
                  },
                  bio_image_url="/buddies/tomas_novau.jpg",
                  email="tomas.novau@endava.com",
                  skype="tnovau"
                }

            };
        }
    }
}
