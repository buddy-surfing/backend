﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endava.BuddySurfing.API.Models;

namespace Endava.BuddySurfing.API.Builders
{
    public static class CitiesBuilder
    {
        public static IList<City> Build()
        {
            return new List<City>()
            {
                new City()
                {
                    city_name="Amsterdam",
                    country_name="Netherlands",
                    continent="Western Europe",
                    image_url = "/cities/small/amsterdam.jpg",
                    image_large_url = "/cities/large/amsterdam.jpg"
                },
                new City()
                {
                    city_name="Atlanta",
                    country_name="United States",
                    continent="North America",
                    image_url = "/cities/small/atlanta.jpg",
                    image_large_url = "/cities/large/atlanta.jpg",
                }
                ,
                new City()
                {
                    city_name="Belgrade",
                    country_name="Serbia",
                    continent="Central Europe",
                    image_url = "/cities/small/belgrade.jpg",
                    image_large_url = "/cities/large/belgrade.jpg",
                },
                new City()
                {
                    city_name="Bogotá",
                    country_name="Colombia",
                    continent="Latin America",
                    image_url = "/cities/small/bogota.jpg",
                    image_large_url = "/cities/large/bogota.jpg",
                },
                new City()
                {
                    city_name="Bucharest",
                    country_name="Romania",
                    continent="Central Europe",
                    image_url = "/cities/small/bucharest.jpg",
                    image_large_url = "/cities/large/bucharest.jpg",
                },
                new City()
                {
                    city_name="Buenos Aires",
                    country_name="Argentina",
                    continent="Latin America",
                    image_url = "/cities/small/buenos_aires.jpg",
                    image_large_url = "/cities/large/buenos_aires.jpg",
                },
                new City()
                {
                    city_name="Caracas",
                    country_name="Venezuela",
                    continent="Latin America",
                    image_url = "/cities/small/caracas.jpg",
                    image_large_url = "/cities/large/caracas.jpg",
                },
                new City()
                {
                    city_name="Chisinau",
                    country_name="Moldova",
                    continent="Central Europe",
                    image_url = "/cities/small/chisinau.jpg",
                    image_large_url = "/cities/large/chisinau.jpg",
                },
                new City()
                {
                    city_name="Cluj-Napoca",
                    country_name="Romania",
                    continent="Central Europe",
                    image_url = "/cities/small/cluj-napoca.jpg",
                    image_large_url = "/cities/large/cluj-napoca.jpg",
                },
                new City()
                {
                    city_name="Copenhagen",
                    country_name="Denmark",
                    continent="Western Europe",
                    image_url = "/cities/small/copenhagen.jpg",
                    image_large_url = "/cities/large/copenhagen.jpg",
                },
                new City()
                {
                    city_name="Denver",
                    country_name="United States",
                    continent="North America",
                    image_url = "/cities/small/denver.jpg",
                    image_large_url = "/cities/large/denver.jpg",
                },
                new City()
                {
                    city_name="Frankfurt",
                    country_name="Germany",
                    continent="Western Europe",
                    image_url = "/cities/small/frankfurt.jpg",
                    image_large_url = "/cities/large/frankfurt.jpg",
                },
                new City()
                {
                    city_name="Iasi",
                    country_name="Romania",
                    continent="Central Europe",
                    image_url = "/cities/small/iasi.jpg",
                    image_large_url = "/cities/large/iasi.jpg"
                },
                new City()
                {
                    city_name="London",
                    country_name="United Kingdom",
                    continent="Western Europe",
                    image_url = "/cities/small/london.jpg",
                    image_large_url = "/cities/large/london.jpg"
                },
                new City()
                {
                    city_name="Medellín",
                    country_name="Colombia",
                    continent="Latin America",
                    image_url = "/cities/small/medellin.jpg",
                    image_large_url = "/cities/large/medellin.jpg"
                },
                new City()
                {
                    city_name="Montevideo",
                    country_name="Uruguay",
                    continent="Latin America",
                    image_url = "/cities/small/montevideo.jpg",
                    image_large_url = "/cities/large/montevideo.jpg"
                },
                new City()
                {
                    city_name="New Jersey",
                    country_name="United States",
                    continent="North America",
                    image_url = "/cities/small/new_jersey.jpg",
                    image_large_url = "/cities/large/new_jersey.jpg"
                },
                new City()
                {
                    city_name="New York",
                    country_name="United States",
                    continent="North America",
                    image_url = "/cities/small/new_york.jpg",
                    image_large_url = "/cities/large/new_york.jpg"
                },
                new City()
                {
                    city_name="Paraná",
                    country_name="Argentina",
                    continent="Latin America",
                    image_url = "/cities/small/parana.jpg",
                    image_large_url = "/cities/large/parana.jpg"
                },
                new City()
                {
                    city_name="Rosario",
                    country_name="Argentina",
                    continent="Latin America",
                    image_url = "/cities/small/rosario.jpg",
                    image_large_url = "/cities/large/rosario.jpg"
                },
                new City()
                {
                    city_name="Seattle",
                    country_name="United States",
                    continent="North America",
                    image_url = "/cities/small/seattle.jpg",
                    image_large_url = "/cities/large/seattle.jpg"
                },
                new City()
                {
                    city_name="Skopje",
                    country_name="Macedonia",
                    continent="Central Europe",
                    image_url = "/cities/small/skopje.jpg",
                    image_large_url = "/cities/large/skopje.jpg"
                },
                new City()
                {
                    city_name="Sofia",
                    country_name="Bulgaria",
                    continent="Central Europe",
                    image_url = "/cities/small/sofia.jpg",
                    image_large_url = "/cities/large/sofia.jpg"
                }
            };
        }
    }
}

