﻿using Endava.BuddySurfing.API.Models;
using System.Collections.Generic;

namespace Endava.BuddySurfing.API.Builders
{
    public static class RecommendationBuilder
    {
        public static IList<Recommendation> Build()
        {
            return new List<Recommendation>()
            {
                new Recommendation()
                {
                    place_id = 1,
                    place_name = "Despensa de Jose",
                    tag_category = new List<string>() {"food"},
                    address = "Salta 2310",
                    description= "The Best Pie in Rosario. Prize and quality !! Awesome!!.",
                    like= 20,
                    city_name = "Rosario"
                },
                new Recommendation()
                {
                    place_id = 2,
                    place_name = "Anne Frank House",
                    tag_category = new List<string>() {"sightseeing"},
                    address = " Prinsengracht 263-267, 1016 GV Amsterdam, Netherlands",
                    description= "Museum house where Anne Frank & her family hid from the Nazis in a secret annex, during WWII.",
                    like= 50,
                    city_name = "Amsterdam"
                },
                new Recommendation()
                {
                    place_id = 3,
                    place_name = "Rijksmuseum",
                    tag_category = new List<string>() {"sightseeing"},
                    address = "Museumstraat 1, 1071 XX Amsterdam, Netherlands",
                    description= "The Rijksmuseum is a Dutch national museum dedicated to arts and history in Amsterdam.",
                    like= 50,
                    city_name = "Amsterdam"
                },
                new Recommendation()
                {
                    place_id = 4,
                    place_name= "Holiday Inn Express",
                    tag_category = new List<string>() {"hosting"},
                    address= "Salta 1940",
                    description= "Great place to sleep!! Personal is super warm & cool.",
                    like= 20,
                    city_name = "Rosario"
                },
                new Recommendation()
                {
                    place_id = 5,
                    place_name= "Wallace",
                    tag_category = new List<string>() {"party", "drinks"},
                    address= "Av. Belgrano 456",
                    description= "Super atmosphere..excellent place to drink and dance",
                    like= 12,
                    city_name = "Rosario"
                },
                new Recommendation()
                {
                    place_id = 6,
                    place_name= "Birra",
                    tag_category = new List<string>() {"beer", "food"},
                    address= "Alvear 54 bis",
                    description = "The best menu of craft beer in all city!! Taste IPA",
                    like= 50,
                    city_name = "Rosario"
                },
                new Recommendation()
                {
                    place_id = 7,
                    place_name = "Marcelo A. Bielsa stadium",
                    tag_category = new List<string>() {"sightseeing"},
                    address= "Av. Int. Morcillo 2501-2699, S2000 Rosario, Santa Fe",
                    description = "The football stadium of the best team in town!",
                    like= 200,
                    city_name = "Rosario"
                },
                new Recommendation()
                {
                    place_id = 7,
                    place_name = "Gigante de Arroyito",
                    tag_category = new List<string>() {"sightseeing","party"},
                    address= "Cordiviola 1100, S2000COQ Rosario, Santa Fe",
                    description = "The football stadium of Rosario Central!",
                    like= 201,
                    city_name = "Rosario"
                },
                new Recommendation()
                {
                    place_id = 8,
                    place_name= "Monumento Nacional a la Bandera",
                    tag_category = new List<string>() {"sightseeing"},
                    address = "Sta Fe 581, S2000ATC Rosario, Santa Fe",
                    description= "Monument to our flag, near to the river and a great park",
                    like= 130,
                    city_name = "Rosario"
                },
                new Recommendation()
                {
                    place_id = 9,
                    place_name= "Acuario del Río Paraná",
                    tag_category = new List<string>() {"sightseeing"},
                    address= "Av. Eduardo Carrasco S/N, 2000 Rosario, Santa Fe",
                    description = "Aquarium of the Paraná river",
                    like= 30,
                    city_name = "Rosario"
                },
                new Recommendation()
                {
                    place_id = 10,
                    place_name= "Rambla Catalunya",
                    tag_category = new List<string>() {"sightseeing"},
                    address= "2592, Av. Eudoro Carrasco 2544, Rosario, Santa Fe",
                    description = "Walk through the river shore with a lot of food trucks. Get a beer at the sunset!",
                    like= 30,
                    city_name = "Rosario"
                },
                new Recommendation()
                {
                    place_id = 12,
                    place_name= "Heineken Experience",
                    tag_category = new List<string>() {"drinks", "sightseeing"},
                    address= "Stadhouderskade 78, 1072 AE Amsterdam, Países Bajos",
                    description = "Interactive tour through beer giant's history in former brewery, with a tasting room finale.",
                    like= 555,
                    city_name = "Amsterdam"
                },
                new Recommendation()
                {
                    place_id = 13,
                    place_name= "Hotel Torenzicht",
                    tag_category = new List<string>() {"hosting","drinks","food"},
                    address= "Oudezijds Achterburgwal 93, 1012 DD Amsterdam, Netherlands",
                    description = "Functional canalside hotel in an art nouveau style property featuring free breakfast & a 24/7 bar.",
                    like= 23,
                    city_name = "Amsterdam"
                },
                new Recommendation()
                {
                    place_id = 14,
                    place_name= "Zaanse Schans",
                    tag_category = new List<string>() {"sightseeing"},
                    address= "Kalverringdijk 1, 1509 BT Zaandam, Netherlands",
                    description = "Zaanse Schans is a neighborhood in the Dutch town of Zaandam, near Amsterdam. Historic windmills and distinctive green wooden houses were relocated here to recreate the look of an 18th/19th-century village. The Zaans Museum has regional costumes, model windmills and interactive exhibits on chocolate making. Artisan workshops demonstrate rare handicrafts such as wooden clog carving, barrel making and pewter casting.",
                    like= 2345,
                    city_name = "Amsterdam"
                }
            };
        }
    }
}
